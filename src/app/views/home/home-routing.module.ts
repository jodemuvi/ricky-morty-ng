import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {RoutesConst} from '../../utils/constants/routes.const';
import {HomeComponent} from './home.component';

export const routes: Routes = [
  {
    path: RoutesConst.Clean,
    component: HomeComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {
}
