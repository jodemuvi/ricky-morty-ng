import {Component, OnInit} from '@angular/core';
import {ApisService} from '../../shared/services/apis.service';
import {Character} from '../../shared/models/internal/character';
import {GeneralConst} from '../../utils/constants/general.const';
import {InfoResponse} from '../../shared/models/base/info.response';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'rm-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  info: InfoResponse = null;
  characters: Character[] = [];
  status = GeneralConst.Status;

  filterControl = new FormControl('');

  constructor(private readonly apisService: ApisService) {
  }

  ngOnInit(): void {
    this.filter();
  }

  filter(url = null) {
    this.getCharacters(url);
  }

  private getCharacters(url = null): void {
    this.apisService.characters(url, this.filterControl.value).subscribe(resp => {
      this.info = resp.info;
      this.characters = resp.results;
    });
  }
}
