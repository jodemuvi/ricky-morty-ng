import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {RoutesConst} from '../utils/constants/routes.const';
import {NavigationGuard} from '../shared/guards/navigation.guard';

export const routes: Routes = [
  {path: RoutesConst.Clean, redirectTo: RoutesConst.Views.Home.path, pathMatch: 'full'},
  {
    path: RoutesConst.Views.Home.path,
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
    data: {title: RoutesConst.Views.Home.title, breadcrumb: RoutesConst.Views.Home.breadcrumb},
    canActivate: [NavigationGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewsRoutingModule {
}
