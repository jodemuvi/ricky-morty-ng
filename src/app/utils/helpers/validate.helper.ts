import {isNumeric} from 'rxjs/internal/util/isNumeric';

export class ValidateHelper {

  public static validBoolean(_value: boolean, _resultNoValid = false): boolean {
    if (_value !== undefined && _value !== null) {
      return _value;
    }
    return _resultNoValid;
  }

  public static validString(_value: any, _case = '', _resultNoValid = ''): string {
    if (_value && _value.toString().trim()) {
      if (_case && (_case === 'upper' || _case === 'lower')) {
        return (_case === 'upper' ? _value.toString().trim().toUpperCase() : _value.toString().trim().toLowerCase());
      }
      return _value.toString().trim();
    }
    return _resultNoValid;
  }

  public static validNumber(_value: any, _resultNoValid: number = null): number {
    if (_value) {
      if (isNumeric(_value)) {
        return Number(_value);
      } else if (_value instanceof String) {
        return Number(_value);
      }
    }
    return _resultNoValid;
  }

  public static validArray(_value: any, _resultNoValid = []): Array<any> {
    if (_value && (_value instanceof Array)) {
      return _value;
    }
    return _resultNoValid;
  }
}
