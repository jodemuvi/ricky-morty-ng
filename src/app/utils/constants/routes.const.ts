export class RoutesConst {
  public static Clean = '';
  public static Views = {
    path: 'app',
    Home: {
      path: 'home',
      fullPath: '/app/home',
      title: 'Inicio',
      breadcrumb: 'Inicio'
    },
    Unauthorized: {
      path: 'no-autorizado',
      fullPath: '/app/no-autorizado'
    },
    ServerError: {
      path: 'error-servidor',
      fullPath: '/app/error-servidor'
    }
  };
}
