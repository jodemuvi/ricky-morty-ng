import {NgModule} from '@angular/core';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';
import {RoutesConst} from './utils/constants/routes.const';

const routes: Routes = [
  {path: RoutesConst.Clean, redirectTo: RoutesConst.Views.path, pathMatch: 'full'},
  {
    path: RoutesConst.Views.path,
    loadChildren: () => import('./views/views.module').then(m => m.ViewsModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true, preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
