import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {finalize, catchError} from 'rxjs/operators';
import {SessionService} from './session.service';
import {LoadingSpinnerService} from '../../components/loading-spinner/loading-spinner.service';

@Injectable({providedIn: 'root'})
export class ExtraService {

  constructor(private http?: HttpClient,
              private sessionService?: SessionService,
              private spinnerService?: LoadingSpinnerService) {
  }

  public getAnyService(_url: string, _params: HttpParams = null, _auth: number = null, _withCredentials = false, _showLoader: boolean = true): Observable<any> {
    this.spinnerShow(_showLoader);
    return this.http.get(_url, {headers: this.headerGlobal(_auth), params: _params, withCredentials: _withCredentials})
      .pipe(finalize(() => {
        this.spinnerHide(_showLoader);
      }), catchError((err) => {
        return throwError(err);
      }));
  }

  public postCustomService(_url: string, _requestBody: any, _headers: HttpHeaders, _params: HttpParams, _withCredentials = false, _showLoader: boolean = true): Observable<any> {
    this.spinnerShow(_showLoader);
    return this.http.post(_url, _requestBody, {headers: _headers, params: _params, withCredentials: _withCredentials})
      .pipe(finalize(() => {
        this.spinnerHide(_showLoader);
      }), catchError((err) => {
        return throwError(err);
      }));
  }

  public postAnyService(_url: string, _requestBody: any, _params: HttpParams, _auth: number = null, _withCredentials = false, _showLoader: boolean = true): Observable<any> {
    this.spinnerShow(_showLoader);
    return this.http.post(_url, _requestBody, {
      headers: this.headerGlobal(_auth),
      params: _params,
      withCredentials: _withCredentials
    })
      .pipe(finalize(() => {
        this.spinnerHide(_showLoader);
      }), catchError((err) => {
        return throwError(err);
      }));
  }

  public getBlobService(_url: string, _params: HttpParams, _auth: number = null, _withCredentials = false, _showLoader: boolean = true): Observable<any> {
    this.spinnerShow(_showLoader);
    return this.http.get(_url, {
      responseType: 'blob',
      headers: this.headerGlobal(_auth),
      params: _params,
      withCredentials: _withCredentials
    })
      .pipe(finalize(() => {
        this.spinnerHide(_showLoader);
      }), catchError(err => {
        return throwError(err);
      }));
  }

  public postMultipartService(_url: string, _requestBody: any, _params: HttpParams, _auth: number = null, _withCredentials = false, _showLoader: boolean = true): Observable<any> {
    this.spinnerShow(_showLoader);
    return this.http.post(_url, _requestBody, {
      headers: this.headerGlobal(_auth, true),
      params: _params,
      withCredentials: _withCredentials
    })
      .pipe(finalize(() => {
        this.spinnerHide(_showLoader);
      }), catchError((err) => {
        return throwError(err);
      }));
  }

  private spinnerShow(showLoader: boolean) {
    if (showLoader) {
      this.spinnerService.show();
    }
  }

  private spinnerHide(showLoader: boolean) {
    if (showLoader) {
      this.spinnerService.hide();
    }
  }

  private headerGlobal(_auth: number, multipart: boolean = false): HttpHeaders {
    if (multipart) {
      return this.sessionService.getHeaderMultipart(_auth);
    } else {
      return this.sessionService.getHeader(_auth);
    }
  }
}
