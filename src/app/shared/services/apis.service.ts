import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {ExtraService} from './global/extra.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {CharacterResponse} from '../models/response/character.response';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApisService {
  private base = environment.apis.base;

  private charactersUrl = this.base + environment.apis.character;

  constructor(private readonly extraService: ExtraService) {
  }

  characters(url = null, name = ''): Observable<CharacterResponse> {
    const param = new HttpParams().set('name', name);
    this.charactersUrl = url ? url : this.charactersUrl;
    return this.extraService.getAnyService(this.charactersUrl, param, null)
      .pipe(map(res => CharacterResponse.createFromObject(res)));
  }
}
