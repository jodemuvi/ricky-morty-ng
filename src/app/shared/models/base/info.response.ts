import {ValidateHelper} from '../../../utils/helpers/validate.helper';

export class InfoResponse {
  count: number;
  pages: number;
  next: string;
  prev: string;

  static createFromObject(obj: any): InfoResponse {
    const newObj = new InfoResponse();
    newObj.count = ValidateHelper.validNumber(obj.count);
    newObj.pages = ValidateHelper.validNumber(obj.pages);
    newObj.next = ValidateHelper.validString(obj.next);
    newObj.prev = ValidateHelper.validString(obj.prev);
    return newObj;
  }
}
