import {InfoResponse} from '../base/info.response';
import {Character} from '../internal/character';

export class CharacterResponse {
  info: InfoResponse;
  results: Character[];

  static createFromObject(obj: any): CharacterResponse {
    const newObj = new CharacterResponse();
    newObj.info = InfoResponse.createFromObject(obj.info);
    newObj.results = Character.createFromObjects(obj.results);
    return newObj;
  }
}
