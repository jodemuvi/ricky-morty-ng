import {ValidateHelper} from '../../../utils/helpers/validate.helper';

export class Character {
  id: number;
  name: string;
  status: string;
  species: string;
  type: string;
  gender: string;
  origin: CharacterLink;
  location: CharacterLink;
  image: string;
  episode: string[];
  url: string;
  created: string;

  static createFromObject(obj: any): Character {
    const newObj = new Character();
    newObj.id = ValidateHelper.validNumber(obj.id);
    newObj.name = ValidateHelper.validString(obj.name);
    newObj.status = ValidateHelper.validString(obj.status);
    newObj.species = ValidateHelper.validString(obj.species);
    newObj.type = ValidateHelper.validString(obj.type);
    newObj.gender = ValidateHelper.validString(obj.gender);
    newObj.origin = CharacterLink.createFromObject(obj.origin);
    newObj.location = CharacterLink.createFromObject(obj.location);
    newObj.image = ValidateHelper.validString(obj.image);
    newObj.episode = ValidateHelper.validArray(obj.episode);
    newObj.url = ValidateHelper.validString(obj.url);
    newObj.created = ValidateHelper.validString(obj.created);
    return newObj;
  }

  static createFromObjects(_objs: any): Array<Character> {
    const newObjs = [];
    if (_objs && (_objs instanceof Array)) {
      for (const item of _objs) {
        newObjs.push(Character.createFromObject(item));
      }
    }
    return newObjs;
  }
}

export class CharacterLink {
  name: string;
  url: string;

  static createFromObject(obj: any): CharacterLink {
    const newObj = new CharacterLink();
    newObj.name = ValidateHelper.validString(obj.name);
    newObj.url = ValidateHelper.validString(obj.url);
    return newObj;
  }
}
