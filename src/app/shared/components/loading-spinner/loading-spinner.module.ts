import {ModuleWithProviders, NgModule} from '@angular/core';
import {LoadingSpinnerComponent} from './loading-spinner.component';
import {LoadingSpinnerService} from './loading-spinner.service';

@NgModule({
  imports: [],
  declarations: [LoadingSpinnerComponent],
  exports: [LoadingSpinnerComponent],
  providers: [LoadingSpinnerComponent]
})
export class LoadingSpinnerModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: LoadingSpinnerModule,
      providers: [LoadingSpinnerService]
    };
  }
}
