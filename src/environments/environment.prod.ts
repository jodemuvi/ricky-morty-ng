export const environment = {
  production: true,
  apis : {
    base: 'http://rickandmortyapi.com',
    character: '/api/character'
  }
};
