export const environment = {
  production: false,
  apis : {
    base: 'http://rickandmortyapi.com',
    character: '/api/character'
  }
};
